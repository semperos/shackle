# Shackle

An experiment in language learning and development.

The `index.html` file in this repo:

* Is self-contained, unstyled, and can be opened in the browser as `file:///path/to/shackle/index.html`, no server necessary.
* Exposes an HTML form for evaluating words in the Shackle language.
* Numbers, strings, and arrays and objects of numbers and strings are interpreted as JSON and pushed onto the stack.
* Symbols (non-numeral + anything else) are looked up in the word dictionary, which is empty to start.
* If the word is not understood, you're prompted to define it.
* Stores the stack and the word dictionary in the URL search params, visible in your browser.

Unimplemented or known-to-be-mis-implemented features:

* Ability to add raw definitions that do not return a value (i.e., that don't put anything on the stack)
* Ability to define words in terms of other words (only raw, JavaScript-based words are currently supported)
* Delayed evaluation (or exposing enough of Shackle to allow user's to implement it)

## Delayed Evaluation

This section contains some background, brainstorm, and considerations for how Shackle might allow the user to use or implement delayed evaluation.

<details>
  <summary>Aside about Delayed Evaluation in Other Languages</summary>
  <p>Languages in the Lisp tradition provide <a href="https://en.wikipedia.org/wiki/Lisp_(programming_language)#List_structure_of_program_code;_exploitation_by_macros_and_compilers">macros</a> that allow you, the language user, to define new Lisp forms that have programmatic control over the <em>code</em> passed into them, including when (or if) to evaluate that code. Lisp macros are essentially functions that you get to define that the Lisp language will call when it's parsing/reading your code.</p>

  <p>Languages in the <a href="https://en.wikipedia.org/wiki/Forth_(programming_language)">Forth</a> tradition (arguably the most well-known example of a stack-based, concatenative language) support a special category of words called <a href="https://en.wikipedia.org/wiki/Forth_(programming_language)#Parsing_words_and_comments">parsing words</a> that, rather than pulling values from the stack, pull their values from the user input (i.e., your source code). For example, the <code>:</code> word reads through your source until it meets the <code>;</code> word and uses everything it finds in between as the definition of a new Forth word. Forth's <code>if</code> is not implemented as a parsing word, but at a high level we can think of it as an advanced parsing word with specific smarts for setting up branches of code execution based on what is on the stack at interpretation time.</p>

  <p>The Factor concatenative language provides <a href="https://docs.factorcode.org/content/article-quotations.html">quotations</a> which act like anonymous functions. The words they wrap can be evaluated by using Factor's <code>call</code> word to invoke the quotation on the top of the stack. Factor's <code>if</code> and other conditionals are built on this foundation. This means that fewer words in the language are parsing words (simplifying one's mental model while reading code), but the language user is required to wrap parts of her program in quotations.</p>
</details>

<p>Shackle provides no such built-in facilities, by design. Some options that come to mind (read the above "Aside about Delayed Evaluation in Other Languages" if you don't know what these terms mean):</p>

<ol>
  <li><em>Quotations:</em> Expose an additional input near the "New Item" one called "New Delayed Item" or "New Quoted Item" for creating quotations. Putting aside UX details, this would involve exposing HTML inputs for entering the words that comprise the quotation. Shackle would then break it's brutal minimalism to include a <code>call</code> word that you would evaluate to invoke a quotation on the top of the stack.
  </li>
  <li><em>Parsing Words:</em> Extend Shackle to allow you to define parsing words. To continue to avoid actual lexing/parsing, this would likely take the form of Shackle exposing an "evaluating" mode (the default) and a "reading" mode. In reading mode, items entered into "New Item" would be collected until your word's delimiter were encountered, at which point your reading word would be invoked.</li>
</ol>

<p>The "Parsing Words" option is the more flexible, since it can be used to implement quotations (assuming Shackle also either exposes a <code>call</code> word or provides a sane way to implement one via Shackle's JavaScript API), but it is arguably more complex and open to abuse (making code that uses custom parsing words itself more complex). At a language design level, this comparison feels like comparing Clojure's closed set of reader macros and Common Lisp's open reader table.</p>

## User Space

This is a collection of thoughts about things that one could do to bend Shackle to your will. Shackle has no user-facing JavaScript API at this point, but the combination of advanced language implementation features and high-level use-cases like this will afford me a basis on which to create an appropriate API.

### Stack Manipulation

Most stack-based languages have a [handful of low-level stack manipulation words](https://docs.factorcode.org/content/article-shuffle-words.html). In addition to the basic ones given below, I also personally reach for `rot ( x y z -- y z x)`.

If you plan to do anything serious with Shackle, you'll want to define these. Have fun determining which ones you _have_ to define in JavaScript and which can be implemented in terms of the others.

#### Removing stack elements

* `drop ( x -- )`
* `2drop ( x y -- )`
* `3drop ( x y z -- )`
* `nip ( x y -- y )`
* `2nip ( x y z -- z )`

#### Duplicating stack elements

* `dup ( x -- x x )`
* `2dup ( x y -- x y x y )`
* `3dup ( x y z -- x y z x y z )`
* `over ( x y -- x y x )`
* `2over ( x y z -- x y z x y )`
* `pick ( x y z -- x y z x )`

### Shackle UI

The sky's the limit here. You can write arbitrary JavaScript functions in your word implementations, so go nuts!

* Customize the look-and-feel of Shackle's interface
* Add a `<canvas>` and implement LOGO-like words to draw things on it
* [Pull down JavaScript from the web](https://stackoverflow.com/a/44807594) and go to town creating canvas, SVG, WebGL, etc. Fun libraries include:
  * [Paper.js](http://paperjs.org/)
  * [p5](https://p5js.org/)
  * [d3](https://d3js.org/)
  * [PixiJS](https://www.pixijs.com/)

_Caveat emptor:_ When you copy and paste a Shackle URL into a new browser session, your whole program is not re-evaluated. Shackle keeps track of the current stack, the next word you want to evaluate, and your word dictionary. This means that any Shackle words you write that produce side-effects (e.g., adding DOM elements, initializing JS frameworks, etc.) will not be run. I may add a feature to Shackle to allow you to easily re-evaluate your program from the beginning so that copying and pasting Shackle URLs that include programs with side-effects will be easier to share, but this is not supported presently.

## License

Copyright (c) 2020 Daniel Gregoire

Source code licensed under [Mozilla Public License Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
